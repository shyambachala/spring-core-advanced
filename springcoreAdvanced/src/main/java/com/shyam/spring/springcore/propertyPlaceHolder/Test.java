package com.shyam.spring.springcore.propertyPlaceHolder;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"com/shyam/spring/springcore/propertyPlaceHolder/config.xml");
		MyDAO bean = (MyDAO) context.getBean("mydao");
		System.out.println(bean);
	}
}
