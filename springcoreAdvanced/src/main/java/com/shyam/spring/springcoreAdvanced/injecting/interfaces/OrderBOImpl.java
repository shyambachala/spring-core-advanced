package com.shyam.spring.springcoreAdvanced.injecting.interfaces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("bo")
public class OrderBOImpl implements OrderBO {

	@Autowired(required=false)
	@Qualifier("dao2")
	private OrderDAO dao;
	
	@Override
	public void placeOrder() {
		System.out.println("Inside the orderBO");
		getDao().createOrder();
	}

	@Override
	public String toString() {
		return "OrderBOImpl [dao=" + getDao() + "]";
	}

	public OrderDAO getDao() {
		return dao;
	}

	public void setDao(OrderDAO dao) {
		this.dao = dao;
	}

}
