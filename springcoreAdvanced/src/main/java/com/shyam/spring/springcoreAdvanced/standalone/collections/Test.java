package com.shyam.spring.springcoreAdvanced.standalone.collections;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext(
				"com/shyam/spring/springcoreAdvanced/standalone/collections/config.xml");
		ProductsList bean = (ProductsList) context.getBean("productList");
		System.out.println(bean);
	}

}
