package com.shyam.spring.springcoreAdvanced.stereotype.annotations;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"com/shyam/spring/springcoreAdvanced/stereotype/annotations/config.xml");
		Instructor instructor = (Instructor) context.getBean("instructor");
		System.out.println(instructor);
	}
}
