package com.shyam.spring.springcoreAdvanced.injecting.interfaces;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"com/shyam/spring/springcoreAdvanced/injecting/interfaces/config.xml");
		OrderBO orderBO = (OrderBO) context.getBean("bo");
		orderBO.placeOrder();
	}
}
