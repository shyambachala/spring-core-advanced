package com.shyam.spring.springcoreAdvanced.autowiring.annotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Employee {

	Employee(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Employee [address=" + getAddress() + "]";
	}

	public Address getAddress() {
		return address;
	}

	@Autowired(required = false)
	@Qualifier("address123")
	private Address address;

}
