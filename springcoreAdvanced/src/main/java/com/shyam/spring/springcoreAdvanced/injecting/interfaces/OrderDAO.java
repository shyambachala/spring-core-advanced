package com.shyam.spring.springcoreAdvanced.injecting.interfaces;

public interface OrderDAO {
	void createOrder();
}
