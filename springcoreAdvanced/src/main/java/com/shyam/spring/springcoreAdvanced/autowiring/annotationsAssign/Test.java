package com.shyam.spring.springcoreAdvanced.autowiring.annotationsAssign;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext(
				"com/shyam/spring/springcoreAdvanced/autowiring/annotationsAssign/config.xml");
		Customer bean = (Customer) context.getBean("customer");
		System.out.println(bean);
	}

}
